import {Navbar, Container, Nav} from 'react-bootstrap'
import {Fragment, useContext} from 'react'
import UserContext from './../UserContext'

export default function AppNavbar(){

	const { user } = useContext(UserContext)
	
	return (
	  <Navbar bg="info" expand="lg">
	      <Container>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link href="/" className="text-white">Home</Nav.Link>
	            <Nav.Link href="/courses" className="text-white">Courses</Nav.Link> 
	            {
	            	user.email != null 
	            	? 
	            	<Nav.Link href="/logout" className="text-white">Logout</Nav.Link> 
	            	:
	            <Fragment>
	            	<Nav.Link href="/login" className="text-white">Login</Nav.Link> 
	            	<Nav.Link href="/register" className="text-white">Register</Nav.Link>
	            </Fragment>

	            }     
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	  </Navbar>
	)
}

