import {Card, Row, Col, Button} from 'react-bootstrap'
import {Fragment} from 'react'


export default function Highlights(){
	return (
	<Fragment>
		<Row className="m-5">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				      Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Study Now Pay Later</Card.Title>
				    <Card.Text>
				     	Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of Our Community</Card.Title>
				    <Card.Text>
				      Lorem, ipsum dolor, sit amet consectetur adipisicing elit. Pariatur reiciendis maiores odit sed commodi dolore. Illum, magnam, dolorum. Ea unde, praesentium molestias minus suscipit recusandae, totam nesciunt ipsam consectetur vitae.
				    </Card.Text>
				  </Card.Body>
				</Card>		
			</Col>
		</Row>
	</Fragment>
	)
}