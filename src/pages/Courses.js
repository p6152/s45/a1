import coursesData from './../mockData/courses'
import CourseCard from './../components/CourseCard'
import {Fragment, useContext} from 'react';
import UserContext from './../UserContext'

export default function Courses(){

	const { user, setUser} = useContext(UserContext)
	console.log(user)


	const courses = coursesData.map(course => {
			return <CourseCard key={course.id} courseProp={course} />
	})

	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}