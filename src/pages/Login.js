import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'


export default function Login(){

	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate()

	useEffect(() => {
		if(email != "" && pw != ""){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = (e) =>{
		e.preventDefault()
		
		fetch('http://localhost:3007/api/users/login', {
			method: "POST",
			headers:{
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem(`Token`, response.token)
				localStorage.setItem(`email`, email)

				alert(`Login Successfully`)

				setEmail("")
				setPW("")


				navigate(`/courses`)
			}else{
				alert(`Incorrect credentials`)
			}
		})
		
	}

	return(
		<Container className="m-5">
			<h1 className="text-center">Login</h1>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => loginUser(e)}>
					  
					  <Form.Group className="mb-3">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    type="text" 
						    value={email}
						    onChange={(e) => {
                                setEmail(e.target.value)}}
						    />
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Password</Form.Label>
						    <Form.Control 
						    type="password" 
						    value={pw}
						    onChange={(e) => {
                               setPW(e.target.value)}}
						    />
					  </Form.Group>

					  <Button 
					  variant="info" 
					  type="submit"
					  disabled={isDisabled}
					  >
					  Login</Button>
					</Form>
				</Col>
			</Row>		
		</Container>
	)
}	