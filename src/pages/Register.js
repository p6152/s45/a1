import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'

export default function Register(){

	const [fN, setFN] = useState("")
	const [lN, setLN] = useState("")
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [cpw, setCPW] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate()

	// useEffect(function, options)
	useEffect(() => {
		// console.log(`render`)
		if(fN !== "" && lN !== "" && email !== "" && pw !== "" && cpw !== "" && (pw == cpw)){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [fN, lN, email, pw, cpw])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('http://localhost:3007/api/users/email-exists',{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)
			if(!response){
				//send request to register
				fetch('http://localhost:3007/api/users/register',{
					method: "POST",
					headers: {
						"Content-type": "application/json"
					},
					body: JSON.stringify({
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw
					})
				})
				.then(response => response.json())
				.then(response => {
					if(response){
						alert(`Registration successful`)
						//redirect 
						navigate(`/login`)
					}else{
						alert(`Try again`)
					}
				})

			}else{
				alert(`Email already exist, try another email`)
			}
		})
	}


	return(
		<Container className="m-5">
			<h1 className="text-center">Register</h1>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => registerUser(e)}>
					  <Form.Group className="mb-3">
						    <Form.Label>First Name</Form.Label>
						    <Form.Control 
						    type="text" 
						    value={fN} 
						    onChange={(e) => {
                                setFN(e.target.value)}}/>
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control 
						    type="text" 
						    value={lN}
						    onChange={(e) => {
                                setLN(e.target.value)}}/>
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    type="text" 
						    value={email}
						    onChange={(e) => {
                                setEmail(e.target.value)}}
						    />
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Password</Form.Label>
						    <Form.Control 
						    type="password" 
						    value={pw}
						    onChange={(e) => {
                               setPW(e.target.value)}}
						    />
					  </Form.Group>

					  <Form.Group className="mb-3">
						    <Form.Label>Confirm Password</Form.Label>
						    <Form.Control 
						    type="password" 
						    value={cpw}
						    onChange={(e) => {
                                setCPW(e.target.value)}}
						    />
					  </Form.Group>

					  <Button 
					  variant="info" 
					  type="submit"
					  disabled={isDisabled}
					  >
					  Submit</Button>
					</Form>
				</Col>
			</Row>		
		</Container>
	)
}